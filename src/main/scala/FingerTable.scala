/**
 * Created by ashish on 10/22/15.
 */

class FingerTable() {

      var fingers:Array[FingerElement] = new Array[FingerElement](DistributedApp.nodesNum)

      def init(): Unit ={
        for(i<- 0 to DistributedApp.nodesNum-1) {
          fingers(i) = new FingerElement()
        }
      }

      def getFinger(i:Int): FingerElement ={
            fingers(i)
      }


}
