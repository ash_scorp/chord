/**
 * Created by ashish on 10/21/15.
 */

import akka.actor.ActorRef

trait Notifications
case class BackEndRegistration(key:Int, identifier:ActorRef)
case class findSuccessor(sKey:Int, mode:Int, hopCount:Int)
case object workerRegister
case object Stop
case object GetKey
case class Join(num:String)
case object AskMessage
case object GetPredecessor
case object Hello
case class SetPredecessor(value:String)
case class Notify(num:String)
case class SetSuccessor(num:String)
case class CheckPredecessor(num:String)
case object Stabilize
case object FixFingers
case class StabilizeCont(num:String)
case class findPredecessor(sKey:Int, rKey:Int, mode:Int, hopCount:Int)
case class Closest_Preceding_Finger(sKey:Int, rKey:Int, mode:Int, hopCount:Int)
case class Return_Successor(source:String, mode:Int, hopCount:Int)
case class SetFinger(nodeID:String, index:Int)
case class findSuccessor1(sKey:Int, index:Int)
case class findPredecessor1(sKey:Int, rKey:Int, index:Int)
case class Closest_Preceding_Finger1(sKey:Int, rKey:Int, index:Int)
case class Return_Successor1(source:String, index:Int)
case class Checker(initial:ActorRef)
case object Init
case class Done(avg:Float)
case object ResolveQuery