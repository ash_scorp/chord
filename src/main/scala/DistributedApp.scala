/**
 * Created by ashish on 10/21/15.
 */

import akka.actor.{ActorRef, ActorSystem}

object DistributedApp {
 var requestNo: Int = 0
 var nodesNum:Int = 0
 var initiator:ActorRef = null
 var max:Int = 0
 var time:Int = 0
 var kill:Int = 0
 def main (args: Array[String]){
  println("The GAME begins")
  nodesNum = args(0).toInt
  requestNo = args(1).toInt
  if(args.length==3){
   println("Gotchaaa")
   time = args(2).toInt
  }
  else if(args.length==4){
   time = args(2).toInt
   kill = args(3).toInt
  }
  val system = ActorSystem("hello")
  println("Distributed App Starting")
  val boss = Chord.start()
  val notifier = Notifier.start()
  ChordNodes.start(nodesNum, boss, notifier, kill)
 }
}
