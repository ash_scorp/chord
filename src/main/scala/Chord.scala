/**
 * Created by ashish on 10/21/15.
 */


import akka.actor.{Actor, ActorRef, ActorSystem, Props}
import scala.collection.immutable.SortedMap
import scala.util.Random


class Chord() extends Actor{

  var nodeList = IndexedSeq.empty[ActorRef]
  var sortedNodeMap = SortedMap[String, ActorRef]()
  var i:Int = 0
  var initial:ActorRef = null
  DistributedApp.max = (math.pow(2,DistributedApp.nodesNum)-1).toInt

  def receive = {

    case BackEndRegistration(key, workerActor) =>
      println("Registering the worker to the nodeList", i)
      //context watch sender()
      i += 1
      nodeList = nodeList :+ sender()
      println("Key is :", key)

      //var pathName = sender().path.name.toString()
      //sortedNodeMap += key -> workerActor
      if (initial == null) {
        println("Max number of nodes allowed is ", DistributedApp.max)
        println("First actor entering: ", sender().path.toString())
        sender() ! Join(null)
        initial = sender()
      }
      else {
        println("New actor coming through ", sender().path.toString())
        val random = new Random()
        val peer = 1 + random.nextInt(5)
        initial ! Join(getActorName(sender().path.toString()))
        //context.system.actorSelection(initial) ! Join(sender().path.toString())
      }

      /*Thread.sleep(10000)
      for(i<-nodeList) {
        i ! Hello
      }*/
      /*while (true) {
      if (nodeList.length == 4) {

        for (i <- nodeList) {
          i ! Hello
          Thread.sleep(10)
          }
        }
      }*/

      /*if (i==99){
        self ! Stop
      }*/

    case Stop =>
      println("Its time to die ")
      stopAllWorkers
      context.system.terminate()
  }

  def stopAllWorkers(): Unit =
  {
    for(worker<-nodeList){
      worker ! Stop
    }
  }

  def getActorName(actorPath :String):String={
    val nodeArr = actorPath.split('/')
    val nodeVal = nodeArr(nodeArr.length-1)
    nodeVal
  }
}



object Chord {
  def start(): ActorRef = {
    //Override the configuration of the port when specified as program argument
    val system = ActorSystem()
    system.actorOf(Props(new Chord()), name = "ChordBoss")
  }
}