# README #

This README documents whatever steps are necessary to get your application up and running.

## Team Members ##
Mebin Jacob (3891-3302), Email-id: mebinjacob@ufl.edu
Ashish Nautiyal (6351-6173), Email-id: an121189@ufl.edu 

### How to Run ###

1. The format to run is : sbt "run Number_of_Nodes Number_of_Requests TimeGap(Optional)"
   So for example, running on 1000 nodes with 3 request per actor, the syntax of run command is :
	sbt "run 1000 3"

   The "TimeGap" parameter is optional. Its unit is in seconds and if you specify it, it will start the message querying after the specified seconds.

### Configuration Set ###

- We introduce each actor into the network with a 10 milliseconds gap
- Each actor after joining the network waits for 1 second and than starts running the Stabilize() and Fix_Finger()
- Both the Stabilize() and Fix_Finger() have been configured to repeat for each actor after 15 milliseconds. 
- Each actor is scheduled to start accpeting requests after 20 seconds of the start of the application. (We believe that this will give enough time for the actors to primarily set their finger table, though we find that this time is not eough when the number of actor increase beyond a threshold.)


### What is working ###

The application is able to find the hop count for requests in O(logN) time for the network with max 1000 nodes. Beyond that the application gets stuck. The probable reason is mentioned in the following section

### Size of the work unit && Observations ###

We increased the Size of the work unit from 10 to 1000. We tried to run it beyond 1000 also, but we found that the finger tables were taking a lot more time to get updated. 

We realise that every actor as soon as it joins the network, initiates a scheduler to call stabilize() and fix_finger() functions after every 15 millieseconds. As the number of nodes in the network increases, so does the communication between each nodes increases. We ran this application in a 4 core machine and so at any moment of time only 4 actors should be working. In order to verify this we ran the htop simulator in linux machine and found that all the 4 cores were running at max. Now for a smaller network, this still works in a reasonable amount of time(like in 1 minute), but for a bigger network this application takes a lot of time as its finger tables are still getting configured.


### Result of : scala project3.scala  ###

Per actor handling 3 requests, 

# Test Case - 1 #

The request were run after a period of 35 seconds after the application was started:

For a network size of 800 nodes, we found an average hopCount of 6.11997
For a network size of 600 nodes, we found an average hopCount of 6.5118647 
For a network size of 500 nodes, we found an average hopCount of 5.905998
For a network size of 100 nodes, we found an average hopCount of 3.090004

# Test Case - 2 #

When we ran the same experiment after a period of 1 minute the result was:

For a network size of 800 nodes, we found an average hopCount of 5.54795
For a network size of 600 nodes, we found an average hopCount of 6.4156528 
For a network size of 500 nodes, we found an average hopCount of 5.5926314
For a network size of 100 nodes, we found an average hopCount of 3.057778

We observe that the stability of each actor has imporved in the second tests, which is obvious since they got more time to stabilize before the requests were made. Hence in order to ensure optimum hopCount, we need to make sure that the nodes in the network are at most optimum stabilized state.
We also observe that only the hopCount of bigger networks was improved significantly in comparison to smaller netowrks. The reason is that the lower networks were already in their near by optimum state in the 1st test case also. 

# Test Case - 3 #

For bigger network of 900, we ran the request handling after a period of 2 minutes:

For a network size of 900 nodes, we found an average hopCount of 6.666665


# Test Case - 4 #

For bigger network of 1000, we ran the request handling after a period of 8-9 minutes:

For a network size of 91000 nodes, we found an average hopCount of 3.386667

We see that the the hopCount for the network size 1000 us less than others, However when we checked the finger tables of the nodes we found that they were not properly configured because of which we somehow ended up with a smaller hopCount. We tried this a couple of times with the same result. We conclude that we need more time for this network size to stabilize. 

