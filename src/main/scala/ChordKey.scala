/**
 * Created by ashish on 10/21/15.
 */


object ChordKey {
  def  createHashKey(identifier:String) : Int = {
    this.synchronized {
        Hash.getHashKey(identifier)
      1
     // Hash.getHashKey()
    }
  }

  def isBetween(key:Int, fromKey:Int, toKey:Int):Boolean = {
    var boolVal:Boolean = false
    if (fromKey.compareTo(toKey) < 0) {
      if (key.compareTo(fromKey) > 0 && key.compareTo(toKey) < 0) {
        boolVal = true
      }
    } else if (fromKey.compareTo(toKey) > 0) {
      if (key.compareTo(toKey) < 0 || key.compareTo(fromKey) > 0) {
        boolVal = true
      }
    }
    else {
      boolVal = false
    }
    boolVal
  }
}
