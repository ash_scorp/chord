import java.nio.ByteBuffer
import java.security.MessageDigest

/**
 * Created by ashish on 10/21/15.
 */

import java.nio.ByteOrder._

object Hash {

  var id:Array[Byte] = null
  var d = 0
 /* def getHashKey(): Int ={
    var k = d
    d+=1
    k
  }*/

  def getHashKey(identifier:String):String={
    var md:MessageDigest = MessageDigest.getInstance("SHA-1")
    md.reset()
    md.update(identifier.getBytes())
    var byte:Array[Byte] = md.digest()
    var buf:ByteBuffer = ByteBuffer.wrap(byte)
    buf.order(LITTLE_ENDIAN)
    val i:Int = buf.getInt
    math.abs(i).toString()
    //toHexString()
  }

  def toHexString() :String = {
    var result:StringBuilder = new StringBuilder()
    for (a <- 0 to this.id.length-1){
      var block:String = Integer.toHexString(this.id(a) & 0xff).toUpperCase()
      if(block.length()<2){
        block = "0" + block
      }
      result.append(block)
    }
    return result.toString()
  }


}
