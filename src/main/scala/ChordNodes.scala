import java.net.{InetAddress, URL}

import akka.actor._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.util.Random
/**
 * Created by ashish on 10/23/15.
 */



class ChordNodes(identifi:String, boss:ActorRef, notifier:ActorRef) extends Actor {
  val nodeID = getActorName(context.self.path.toString())
  val identifier = identifi
  val nodeKey = nodeID.toInt
  var predecessor: String = null
  var successor: String = null
  var fingers: FingerTable = new FingerTable()
  var vStabilize: Cancellable = null
  var vFixFingers: Cancellable = null
  var vMessages:Cancellable = null
  var showContent: Cancellable = null
  val heartBeat: FiniteDuration = 15 milliseconds
  var counter:Int = 0
  var scheduly:Boolean = false
  var hops:Int = 0
  var reqProcessed:Int =0
  var mCounter = 0


  def receive = {

    case Hello =>
      mCounter+=1
      if(mCounter>1){
        if(!vMessages.isCancelled) {
          vMessages.cancel()
        }
      }
      else {
        println(" *******Actor******* ", nodeID, " Predecessor ", predecessor, " Successor ", successor)
      }

    case ResolveQuery =>
      val random = new Random()
      //while(mCounter<DistributedApp.requestNo) {
        var query = random.nextInt(DistributedApp.max)
        var hopCount = 0
        self ! findSuccessor(query, 1, hopCount)
        mCounter+=1
      //}
      /*if(!vMessages.isCancelled){
        vMessages.cancel()
      }*/

    case Init =>
      if(predecessor==null){
          predecessor = nodeID
      }
      //println("^^^^^^^^^^^^^^^^^^^INIT^^^^^^^^^^^^^^^^^^^^^^")
      vStabilize = context.system.scheduler.schedule(1000 milliseconds, heartBeat, self, Stabilize)
      vFixFingers = context.system.scheduler.schedule(1000 milliseconds, heartBeat, self, FixFingers)
      //if(self==DistributedApp.initiator) {
      var timer:Int = 20
      if(DistributedApp.time!=0){
        timer = DistributedApp.time
      }
        vMessages = context.system.scheduler.schedule(timer seconds, 1 second, self, ResolveQuery)
      //}

    case CheckPredecessor(num) =>
      //println("Came for actor ", nodeID, " on behalf of ", num, " . Current predecessor is ", predecessor)
      if (predecessor == null || predecessor == nodeID) {
        predecessor = num
        //println(nodeID, " Setting predecessor ", predecessor)
      }
      else{
        if(checkInExcBetween(num.toInt,predecessor.toInt,nodeKey)){
          predecessor = num
          //println(nodeID, " Setting predecessor ", predecessor)
        }
      }

    case FixFingers =>
      if(counter<DistributedApp.nodesNum*80) {
        if (DistributedApp.nodesNum > 1) {
          val random = new Random()
          val index = 1 + random.nextInt(DistributedApp.nodesNum - 1)
          val startVal = ((nodeKey + math.pow(2, index)) % math.pow(2, DistributedApp.max)).toInt
          self ! findSuccessor1(startVal, index)
        }
        counter+=1
      }
      else{
        if(!vFixFingers.isCancelled) {
          vFixFingers.cancel()
        }
      }

    case SetSuccessor(num) =>
      if(!scheduly){
        self ! Init
        scheduly = true
      }
      var found:Boolean = true
      if(successor!=nodeID && successor!=null){
        found = false
        if(checkInExcBetween(num.toInt,nodeKey,successor.toInt)){
          found= true
        }
      }
      if(found==true) {
        successor = num
        if (fingers.getFinger(0) == null) {
          fingers.init()
        }
        fingers.getFinger(0).setStart(((nodeKey + math.pow(2, 0)) % math.pow(2, DistributedApp.nodesNum)).toInt)
        fingers.getFinger(0).setNode(successor)
        //println(nodeID, " Setting successor ", successor)
        context.actorSelection("../" + successor.toString()) ! CheckPredecessor(nodeID)
      }

    case GetPredecessor =>
      sender() ! StabilizeCont(predecessor)

    case StabilizeCont(pred) =>
      //println("The predecessor obtained was", pred)
      if (pred != null) {
        if (checkInExcBetween(pred.toInt,nodeKey,successor.toInt)) {
          //println("I entered the if check")
          successor = pred
          fingers.getFinger(0).setNode(pred)
        }
        else {
          //println("Didnt entered the check")
        }
      }
      //println(nodeID, " Informaing my successor ", successor )
      if(successor!=null) {
        context.actorSelection("../" + successor) ! Notify(nodeID)
      }


    case Notify(num) =>
      //println("Notify : For Actor", nodeID, " num : ", num)
      if (predecessor == null || (checkInExcBetween(num.toInt,predecessor.toInt,nodeKey))) {
        predecessor = num
        //println("Setting predecessor ", predecessor)
      }


    case Stabilize =>
      //println("Stabilizer running")
      if(counter<DistributedApp.nodesNum*80) {
        //println("Stabilizing starting from ", nodeID)
        if(successor!=null) {
          context.actorSelection("../" + successor) ! GetPredecessor
        }
        counter+=1
      }
      else{
        if(!vStabilize.isCancelled) {
          vStabilize.cancel()
        }
      }


    case findPredecessor1(sKey, rKey, index) =>
      //println("Skey is ",sKey, ", NodeKey is ", nodeKey, " Successor is ", successor)
      if(successor!=null) {
        if (checkInBetween(sKey, nodeKey, successor.toInt)) {
          //println("Finally Came here")
          context.actorSelection("../" + rKey) ! Return_Successor1(successor, index)
        }
        else {
          self ! Closest_Preceding_Finger1(sKey, rKey, index)
        }
      }

    case Closest_Preceding_Finger1(sKey, rKey, index) =>
      var found = false
      for (i <- DistributedApp.nodesNum - 1 to 0 by -1) {
        if (found == false) {
          if (fingers.getFinger(i).getNode() != null) {
            if (checkInExcBetween(fingers.getFinger(i).getNode().toInt, nodeKey, sKey)) {
              context.actorSelection("../" + fingers.getFinger(i).getNode()) ! findPredecessor1(sKey, rKey, index)
              found = true
              //println("Found the match")
            }
          }
        }
      }
      if (found == false) {
        self ! findPredecessor1(sKey, rKey, index)
      }

    case Return_Successor1(source, index) =>
      if(source.toInt>DistributedApp.nodesNum){
        //println("DEAD LETTERS WILL BE ENCOUNTERED : ", source)
      }
      self ! SetFinger(source, index)

    case findSuccessor1(sKey, index) =>
      //println("******************Successor Find *******************")
      //println("Node ", nodeID)
      //println("Successor ", successor)
      self ! findPredecessor1(sKey, nodeKey, index)

    case SetFinger(node, index) =>
      fingers.getFinger(index).setNode(node)
      fingers.getFinger(index).setStart(((nodeKey + math.pow(2, index)) % math.pow(2, DistributedApp.nodesNum)).toInt)
      //println("***************** Setting for ", nodeID, " with index: ", index, " Finger Node: ", fingers.getFinger(index).getNode())

    case findPredecessor(sKey, rKey, mode, hopCount) =>
      if(mode==1){
        //println("Current Node: ", nodeID)
        //StdIn.readChar()
      }
      if (nodeID == successor) {
        context.actorSelection("../" + rKey) ! Return_Successor(sKey.toString(), mode, hopCount)
        self ! SetSuccessor(sKey.toString())
      }
      else if (successor!=null && checkInBetween(sKey, nodeKey, successor.toInt)) {
        //println("Finally Came here")
          context.actorSelection("../" + rKey) ! Return_Successor(sKey.toString(), mode, hopCount)
      }
      else {
        val nHopCount=hopCount+1
        self ! Closest_Preceding_Finger(sKey, rKey, mode, nHopCount)
      }

    case Closest_Preceding_Finger(sKey, rKey, mode, hopCount) =>
      if(mode==1){
        //println("Successor Find for ", sKey, "by Node ", nodeID, " having successor", successor)
      }
      var found = false
      for (i <- DistributedApp.nodesNum - 1 to 0 by -1) {
        if (found == false) {
          if (fingers.getFinger(i).getNode() != null) {
            if (checkInExcBetween(fingers.getFinger(i).getNode().toInt, nodeKey, sKey)) {
              if(mode==1){
                //println("Yes we got in")
                //StdIn.readChar()
              }
              context.actorSelection("../" + fingers.getFinger(i).getNode()) ! findPredecessor(sKey, rKey, mode, hopCount)
              found = true
              //println("Found the match")
            }
          }
        }
      }
      if (found == false) {
        self ! findPredecessor(sKey, rKey, mode, hopCount)
      }

    case Return_Successor(source, mode, hopCount) =>
      mode match {
        case 0 =>
          context.actorSelection ("../" + source) ! SetSuccessor (nodeID)
        case 1 =>
          //println("Return Successor ", DistributedApp.requestNo, " Current value ", reqProcessed, " This node: ", nodeID)
          hops+=hopCount
          reqProcessed+=1
          if(reqProcessed==DistributedApp.requestNo){
            if(!vMessages.isCancelled){
              vMessages.cancel()
            }
            val avg = hops/DistributedApp.requestNo.toFloat
            //println("The avg hop count is ", avg, " Request Processed", reqProcessed)
            notifier ! Done(avg)
          }


      }

    case findSuccessor(sKey, mode, hopCount) =>
      if(mode==1){
        //println("Mode 1 : Successor Find for ", sKey, "by Node ", nodeID, " having successor", successor)
      }
      self ! findPredecessor(sKey, nodeKey, mode, hopCount)

    case Checker(initial) =>
      initial ! Stop

    case Stop =>
      println("%%%%%%%%%%%%% Sutting Down %%%%%%%%%%%%%%%%%%% ",self)
      context.actorSelection("../"+predecessor) ! SetSuccessor(successor)
      context.actorSelection("../"+successor) ! SetPredecessor(predecessor)
      context.system.shutdown()

    case Join(num) =>
      if (num == null) {
        //println("Dude you have reached the first node")
        if (fingers.getFinger(0) == null) {
          fingers.init()
        }
        fingers.getFinger(0).setStart(((nodeKey + math.pow(2, 0)) % math.pow(2, DistributedApp.nodesNum)).toInt)
        fingers.getFinger(0).setNode(nodeID)
        successor = nodeID
        predecessor = nodeID
        if(!scheduly){
          self ! Init
          scheduly = true
        }

       //showContent = context.system.scheduler.schedule(20 second, heartBeat, self, Hello)
      }
      else {
        var hopCount=0
        //println("We need to find the successor for ", num)
        //context.actorSelection("../"+successor) ! findSuccessor(num.toInt, nodeKey)
        self ! findSuccessor(num.toInt, 0, hopCount)
      }


    case workerRegister =>
      //println("Working node",this)
      boss ! BackEndRegistration(nodeKey, self)
  }

  def getActorName(actorPath: String): String = {
    val nodeArr = actorPath.split('/')
    val nodeVal = nodeArr(nodeArr.length - 1)
    nodeVal
  }

  def checkInBetween(sKey: Int, start: Int, rKey: Int): Boolean = {
    if(start <= rKey) {
      (start <= sKey && sKey <= rKey)
    }
    else{
      (sKey > start && sKey <= DistributedApp.max) || (sKey >= 0 && sKey < rKey)
    }
  }

  def checkInExcBetween(sKey: Int, lkey: Int, rKey: Int): Boolean = {
    if (lkey < rKey) {
      (lkey < sKey && sKey < rKey)
    }
    else {
      (sKey > lkey && sKey <= DistributedApp.max) || (sKey >= 0 && sKey < rKey)
    }
  }
}



object ChordNodes{
  def start(n:Int, master: ActorRef, notifier:ActorRef, kill:Int): Unit = {
    val system = ActorSystem() //"ClusterSystem", config
    var host:String = InetAddress.getLocalHost.getHostAddress()
    var port:Int = 9000
    var initial:String = null
    var nodeList = IndexedSeq.empty[ActorRef]
    for (workerNo <- 1 to n) {
      if(true) {
        Thread.sleep(10)
      }
      var url:URL = new URL("http",host,port+workerNo,"")
      val workerName = Hash.getHashKey(url.toString())
      //println(workerNo," ", url.toString())

      var workerActor = system.actorOf(Props(new ChordNodes(url.toString(), master, notifier)), name = workerName)
      if(workerNo==1){
        initial = workerName.toString()
        DistributedApp.initiator = workerActor
      }
      nodeList = nodeList :+ workerActor
      workerActor ! workerRegister
    }
  }
}