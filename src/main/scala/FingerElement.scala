/**
 * Created by ashish on 10/22/15.
 */

class FingerElement {
  var start:Int = 0
  var node:String = null // node is actually actor path

  def setStart(key:Int)={
    start = key
  }
  def setNode(node:String)={
    this.node = node
  }
  def getStart(): Int ={
    start
  }
  def getNode(): String = {
    node
  }
}
