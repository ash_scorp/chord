import akka.actor.{Actor, ActorRef, ActorSystem, Props}

/**
 * Created by ashish on 10/25/15.
 */
class Notifier extends Actor{
  var total:Float = 0
  var counter:Int = 0
  var set:Boolean = false
  var aAvg:Float = 0
  def receive = {
    case Done(avg) =>
      total += avg
      counter += 1
      if(!set) {
        println(" Average Hops from Sender", sender(), " : " + avg, "Counter is : ", counter)
        if (counter >= DistributedApp.nodesNum ) {
          if (!set) {
            aAvg = total / counter
          }
          set = true
          println(" Average Hops  " + aAvg)
        }
      }
  }
}

object Notifier{
  def start(): ActorRef = {
    //Override the configuration of the port when specified as program argument
    val system = ActorSystem()
    system.actorOf(Props(new Notifier()), name = "Notifier")
  }
}
